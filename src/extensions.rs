// extensions.rs: extension list handling sub-routines
//
// Copyright 2019 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
//
// This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
// you are welcome to redistribute it and/or modify it under the terms of the
// BSD 2-clause License. See the LICENSE file for more details.
//

use std::cmp::Ordering;
use std::collections::HashMap;

type ExtCount<'a> = (&'a &'a str, &'a u32);
type ExtMap<'a> = HashMap<&'a str, u32>;

pub fn count<'a>(mut acc: ExtMap<'a>, extension: &'a String) -> ExtMap<'a> {
    let ext = extension.as_str();

    if extension.is_empty() {
        return acc;
    } else if acc.contains_key(ext) {
        acc.insert(ext, acc[ext] + 1);
    } else {
        acc.insert(ext, 1);
    }

    acc
}

pub fn sort(a: &ExtCount, b: &ExtCount) -> Ordering {
    match b.1.cmp(&a.1) {
        Ordering::Equal => a.0.cmp(&b.0),
        ord => ord,
    }
}
