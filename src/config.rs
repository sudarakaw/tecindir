// config.rs: application configuration sub-routines
//
// Copyright 2019 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
//
// This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
// you are welcome to redistribute it and/or modify it under the terms of the
// BSD 2-clause License. See the LICENSE file for more details.
//

extern crate directories;

use directories::ProjectDirs;
use serde::{Deserialize, Serialize};
use serde_json as json;
use std::fs::{self, File};
use std::io::{self, ErrorKind, Read, Write};

#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    pub max_depth: u8,
    pub max_icons: u8,
    pub ignore_dot_files: bool,
}

impl Default for Config {
    fn default() -> Config {
        Config {
            max_depth: 3,
            max_icons: 3,
            ignore_dot_files: true,
        }
    }
}

fn create_default_db(path: &std::path::Path) -> io::Result<File> {
    match json::to_string_pretty(&Config::default()) {
        Err(_) => Err(io::Error::new(
            ErrorKind::Other,
            "Failed to serialize default configuration",
        )),
        Ok(content) => {
            let file = File::create(path).and_then(|mut f| f.write_all(content.as_bytes()));

            if file.is_err() {
                return Err(io::Error::new(
                    ErrorKind::Other,
                    "Failed to write default configuration to file",
                ));
            }

            File::open(path)
        }
    }
}

fn get_file(dir: &ProjectDirs) -> io::Result<File> {
    let path = &dir.config_dir().join("config.json");

    File::open(&path).or_else(|err| {
        // Failed to open existing file;
        if err.kind() == ErrorKind::NotFound {
            // If file not exists, attempt to create the full path, file
            // & recover from the previous error state.
            fs::create_dir_all(dir.config_dir()).and_then(|_| create_default_db(&path))
        } else {
            Err(err)
        }
    })
}

pub fn load() -> Config {
    let cfg_result = ProjectDirs::from("org", "sudaraka", "tecindir")
        .ok_or(io::Error::new(
            ErrorKind::Other,
            "Unable to locate config directory.",
        ))
        .and_then(|dir| get_file(&dir))
        .and_then(|mut f| {
            let mut text = String::new();

            f.read_to_string(&mut text)?;

            Ok(text)
        })
        .and_then(|text| {
            serde_json::from_str::<Config>(&text)
                .or_else(|err| Err(io::Error::new(ErrorKind::Other, err.to_string())))
        });

    match cfg_result {
        Err(err) => {
            eprintln!("Unable to load configuration.");
            eprintln!("{}", err.to_string());

            Config::default()
        }
        Ok(db) => db,
    }
}
