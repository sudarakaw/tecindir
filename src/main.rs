// main.rs: main entry point for tecindir CLI tool
//
// Copyright 2019 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
//
// This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
// you are welcome to redistribute it and/or modify it under the terms of the
// BSD 2-clause License. See the LICENSE file for more details.
//

use std::collections::HashMap;
use std::env::current_dir;
use tecindir::dir::{self, DirectoryParseContext};
use tecindir::{config, db, extensions};

fn main() {
    let db = db::load();

    current_dir()
        .map(|dir| match db.get_dir_icon(&dir) {
            None => {
                let cfg = config::load();
                let content = dir::parse(
                    &current_dir().unwrap(),
                    &DirectoryParseContext::new(&cfg, &db),
                );
                let type_map = content.iter().fold(HashMap::new(), extensions::count);
                let mut type_count: Vec<_> = type_map.iter().collect();

                type_count.sort_by(extensions::sort);

                if 1 > type_count.len() {
                    println!("{}", db.get_fallback_icon());
                } else {
                    println!(
                        "{}",
                        type_count
                            .iter()
                            .take(cfg.max_icons as usize)
                            .filter_map(|(ft, _)| db.get_type_icon(ft))
                            .collect::<Vec<_>>()
                            .join("")
                            .trim()
                    );
                }
            }
            Some(icon) => println!("{}", icon),
        })
        .unwrap();
}
