// db.rs: application data sub-routines
//
// Copyright 2019 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
//
// This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
// you are welcome to redistribute it and/or modify it under the terms of the
// BSD 2-clause License. See the LICENSE file for more details.
//

extern crate directories;

use directories::{ProjectDirs, UserDirs};
use serde::{Deserialize, Serialize};
use serde_json as json;
use std::collections::HashMap;
use std::fs::{self, File};
use std::io::{self, ErrorKind, Write};
use std::path::PathBuf;

#[derive(Serialize, Deserialize)]
pub struct Data {
    fallback: String,
    types: HashMap<String, Vec<String>>,
    icons: HashMap<String, String>,
    special_dirs: HashMap<String, String>,
}

impl Default for Data {
    fn default() -> Data {
        let mut types = HashMap::new();
        let mut icons = HashMap::new();
        let mut special_dirs = HashMap::new();
        let fallback = String::from("");

        // Extension types

        types.insert(String::from("coffeescript"), vec![String::from("coffee")]);

        types.insert(
            String::from("css"),
            vec![
                String::from("css"),
                String::from("less"),
                String::from("sass"),
                String::from("scss"),
            ],
        );

        types.insert(String::from("elm"), vec![String::from("elm")]);

        types.insert(
            String::from("html"),
            vec![
                String::from("ejs"),
                String::from("hbs"),
                String::from("htm"),
                String::from("html"),
                String::from("mustache"),
            ],
        );

        types.insert(
            String::from("javascript"),
            vec![String::from("js"), String::from("mjs")],
        );

        types.insert(
            String::from("perl"),
            vec![String::from("pl"), String::from("perl")],
        );

        types.insert(String::from("php"), vec![String::from("php")]);

        types.insert(String::from("python"), vec![String::from("py")]);

        types.insert(String::from("rust"), vec![String::from("rs")]);

        types.insert(String::from("shellscript"), vec![String::from("sh")]);

        types.insert(String::from("webassembly"), vec![String::from("wat")]);

        // ICONS

        icons.insert(String::from("coffeescript"), String::from("☕"));
        icons.insert(String::from("css"), String::from("💙"));
        icons.insert(String::from("elm"), String::from("🌱"));
        icons.insert(String::from("html"), String::from("🧡"));
        icons.insert(String::from("javascript"), String::from("💛"));
        icons.insert(String::from("nodejs"), String::from("💚"));
        icons.insert(String::from("perl"), String::from("🐪"));
        icons.insert(String::from("php"), String::from("🐘"));
        icons.insert(String::from("python"), String::from("🐍"));
        icons.insert(String::from("ruby"), String::from("💎"));
        icons.insert(String::from("rust"), String::from("🦀"));
        icons.insert(String::from("shellscript"), String::from("🐚"));
        icons.insert(String::from("webassembly"), String::from("🕸 "));

        // Special directory icons

        if let Some(ud) = UserDirs::new() {
            // Home
            special_dirs.insert(
                String::from(ud.home_dir().to_str().unwrap()),
                String::from("🏠"),
            );

            // Desktop
            special_dirs.insert(
                String::from(ud.home_dir().join("Desktop").to_str().unwrap()),
                String::from("🖥"),
            );

            // Downloads
            special_dirs.insert(
                String::from(ud.home_dir().join("Downloads").to_str().unwrap()),
                String::from("📥"),
            );

            // Documents
            special_dirs.insert(
                String::from(ud.home_dir().join("Documents").to_str().unwrap()),
                String::from("📚"),
            );
        }

        Data {
            fallback,
            types,
            icons,
            special_dirs,
        }
    }
}

impl Data {
    // Return icon type for given file extension
    pub fn get_type(&self, extension: &str) -> Option<String> {
        self.types
            .iter()
            .filter_map(|(extension_type, extension_list)| {
                if extension_list.contains(&String::from(extension)) {
                    Some(extension_type.clone())
                } else {
                    None
                }
            })
            .collect::<Vec<_>>()
            .pop()
    }

    pub fn get_type_icon(&self, filetype: &str) -> Option<String> {
        self.icons
            .get(filetype)
            .map(|ico| String::from(ico.as_str()))
    }

    pub fn get_dir_icon(&self, path: &PathBuf) -> Option<String> {
        self.special_dirs
            .get(path.to_str().unwrap())
            .map(|ico| String::from(ico.as_str()))
    }

    pub fn get_fallback_icon(&self) -> &String {
        &self.fallback
    }
}

fn create_default_db(path: &std::path::Path) -> io::Result<String> {
    match json::to_string_pretty(&Data::default()) {
        Err(_) => Err(io::Error::new(
            ErrorKind::Other,
            "Failed to serialize default data set",
        )),
        Ok(content) => {
            let file = File::create(path).and_then(|mut f| f.write_all(content.as_bytes()));

            if file.is_err() {
                return Err(io::Error::new(
                    ErrorKind::Other,
                    "Failed to write default data set to file",
                ));
            }

            Ok(content)
        }
    }
}

pub fn load() -> Data {
    let db_result = ProjectDirs::from("org", "sudaraka", "tecindir")
        .ok_or(io::Error::new(
            ErrorKind::Other,
            "Unable to locate data directory.",
        ))
        .and_then(|dir| {
            let path = dir.data_dir().join("db.json");

            fs::read_to_string(&path).or_else(|err| {
                // Failed to open existing file;
                if err.kind() == ErrorKind::NotFound {
                    // If file not exists, attempt to create the full path, file
                    // & recover from the previous error state.
                    fs::create_dir_all(dir.data_dir()).and_then(|_| create_default_db(&path))
                } else {
                    Err(err)
                }
            })
        })
        .and_then(|text| {
            serde_json::from_str::<Data>(&text)
                .or_else(|err| Err(io::Error::new(ErrorKind::Other, err.to_string())))
        });

    match db_result {
        Err(err) => {
            eprintln!("Unable to load data.");
            eprintln!("{}", err.to_string());

            Data::default()
        }
        Ok(db) => db,
    }
}
